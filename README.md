# Справочник по различным командам

## git

* Показать содержимое файла на ревизии: `git show branch:path/to/file`
  * Показать состояние файла в **vim**, включить в **vim** нужный синтаксис: 
    ```bash
    git show oldBranch:www/index.php | vim -c 'set syntax=php' -
    ```

* Восстановить файл в состояние нужной ветки или ревизии
  ```bash
  git checkout [branchName|hash] -- path/to/file
  ```

* Обновить неактивную локальную ветку из удалённого репозитория
  ```bash
  git fetch origin master:master
  ```
* Удалить неиспользуемые remote ветки (те, которые `git branch -r`). Удалятся только те remote-ветки, для которых нет соответствующих веток в репозитории `origin`. Например, ветка `origin/master` не удалится
  ```bash
  git remote prune origin
  ```



## ssh
* Копирование папки с удаленного сервера на локальную машину по **ssh** с промежуточным архивированием
  ```bash
  ssh host 'tar -cz -C /remote/path/to/parent target_dir' | tar -xz -C /local/path
  ```
  
* Проброс портов
  
    * Проброс на внутренний порт 
    
        ```bash
        ssh -L 8080:localhost:80 user@remote-server
        ```
    
    * Проброс на удалённую машину, доступную с сервера
    
        ```bash
        ssh -L 8080:another-server:80 user@remote-server
        ```
        
    * Проброс **SOCKS proxy** через ssh. В настройках браузера указать **SOCKS proxy**: `localhost:8890`
    
        ```bash
        # cli
        ssh -D 8890 user@remote-server
        ```

        ```bash
        # ~/.ssh/config
        Host socks-proxy
            HostName remote-server
            User user
            DynamicForward 8890
      ```



## grep
* Поиск по содержимому с пропуском файлов и каталогов
  ```bash
  grep -rl --exclude '*.min.js' --exclude '*.map.js' --exclude-dir 'imopenlines_widget' 'onLocalStorageSet' www/bitrix/js
  ```
* Поиск только в файлах определённого типа
  ```bash
  grep -rl --include \*.sql SELECT www/bitrix/modules
  ```



## интересности bash
* Вызов предыдущей команды с заменой строки `^old_string^new_string`

  ```bash
    sudo -u www-data git status
    # вызов предыдущей команды от имени другого пользователя
    ^www-data^user
  ```



## sudo

* разрешение на запуск команды от имени пользователя

  ```bash
  # sudo visudo
  # разрешение запускать git от имени www-data без пароля: sudo -u www-data git
  current_user   ALL=(www-data)   NOPASSWD: /usr/bin/git
  ```

  


